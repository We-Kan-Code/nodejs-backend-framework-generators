import { ValidateNested, IsNotEmpty, IsDefined } from 'class-validator';
import { messages } from 'resources/strings/configuration/database';

class MongoDB {
  @IsDefined({ message: messages.validation.database.host.empty })
  @IsNotEmpty({ message: messages.validation.database.host.valid })
  public host!: string;

  @IsDefined({ message: messages.validation.database.name.empty })
  @IsNotEmpty({ message: messages.validation.database.name.valid })
  public database!: string;
}

class Database {
  @ValidateNested()
  public mongodb!: MongoDB;
}

export { Database, MongoDB };
