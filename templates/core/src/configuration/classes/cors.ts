import configuration from 'configuration/manager';

/**
 *
 */
class Cors {
  /**
   *
   * @param origin
   * @param callback
   */
  public corsHandler(origin: any, callback: any): Function {
    const { whitelist } = configuration.cors;
    if (!origin) return callback(null, true);

    if (whitelist.indexOf(origin) === -1) {
      const msg =
        'The CORS policy for this site does not ' +
        'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }

    return callback(null, true);
  }
}

export default Cors;
