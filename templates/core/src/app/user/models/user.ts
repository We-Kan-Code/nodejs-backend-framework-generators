import {
  IsDefined,
  IsEmail,
  // IsOptional,
  IsUrl,
  MaxLength,
} from 'class-validator';
import { prop, post, Typegoose, index } from 'typegoose';
import { messages } from 'resources/strings/app/user';
import ErrorHandler from 'middleware/error-handler';

@index({ email: 1 }, { unique: true })
@post('save', ErrorHandler.mongo('User'))
@post('findOneAndUpdate', ErrorHandler.mongo('User'))
class User extends Typegoose {
  @prop()
  @IsDefined({
    groups: ['create'],
    message: messages.errors.validate.firstName.empty,
  })
  @MaxLength(50, {
    groups: ['create', 'update'],
    message: messages.errors.validate.firstName.maxLength,
  })
  public firstName?: string;

  @prop()
  @IsDefined({
    groups: ['create'],
    message: messages.errors.validate.lastName.empty,
  })
  @MaxLength(50, {
    groups: ['create', 'update'],
    message: messages.errors.validate.lastName.maxLength,
  })
  public lastName?: string;

  @prop({ required: true, unique: true })
  @IsDefined({
    groups: ['auth', 'create'],
    message: messages.errors.validate.email.empty,
  })
  @IsEmail(
    {},
    {
      groups: ['auth', 'create', 'update'],
      message: messages.errors.validate.email.invalid,
    },
  )
  public email!: string;

  @prop({ required: true })
  @IsDefined({
    groups: ['auth', 'create'],
    message: messages.errors.validate.password.empty,
  })
  public password!: string;

  @prop({ default: null })
  public tmpPassword?: string | null;

  @prop({ default: null })
  public permissions?: string;

  @prop({ default: null })
  @IsUrl(
    {
      /* eslint-disable */
      require_protocol: true,
      require_valid_protocol: true,
      protocols: ['http', 'https'],
      require_tld: true,
      /* eslint-enable */
    },
    {
      groups: ['create', 'update'],
      message: messages.errors.validate.profileImage.invalid,
    },
  )
  public profileImageUrl?: string;

  @prop({ default: null })
  public refreshToken?: string;

  @prop({ default: null })
  public verificationCode?: string;

  @prop({ default: 0 })
  public status?: number;
}

const UserModel = new User().getModelForClass(User, {
  schemaOptions: {
    id: false,
    versionKey: false,
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
  },
});

export { User, UserModel };
