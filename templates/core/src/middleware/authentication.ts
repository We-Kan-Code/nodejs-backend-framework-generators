import { ExpressMiddlewareInterface } from 'routing-controllers';
import { Response, NextFunction } from 'express';
import { Strategy as JWTStrategy } from 'passport-jwt';
import { UnauthorizedError, ValidationError } from '@wekancompany/common';
import { UserModel, User } from 'app/user/models/user';
import configuration from 'configuration/manager';
import passport from 'passport';
import { messages } from 'resources/strings/middleware/authentication';
import { userStatus } from 'common/enums';
import { IDecodedToken } from './interfaces/authentication';

class Authentication implements ExpressMiddlewareInterface {
  private readonly log = configuration.log.logger;

  public async init(): Promise<void> {
    passport.use(await this.strategy());
  }

  protected async strategy(): Promise<JWTStrategy> {
    return new JWTStrategy(
      {
        jwtFromRequest: (req): any =>
          String(req.headers.authorization).split(' ')[1],
        secretOrKey: configuration.authentication.token.secret,
      },
      (jwtPayload, done): any => {
        // return done(null, {});
        if (jwtPayload.exp === undefined)
          return done(new UnauthorizedError(messages.token.response.invalid));

        if (Date.now() > jwtPayload.exp)
          return done(new UnauthorizedError(messages.token.response.expired));

        return UserModel.findOne({ email: jwtPayload.email })
          .lean()
          .exec()
          .then((user: User): any => {
            if (!user) {
              this.log.warn(
                messages.login.failed +
                  jwtPayload.email +
                  messages.token.invalid,
              );
              return done(
                new UnauthorizedError(messages.token.response.invalid),
              );
            }
            if (user.status === userStatus.notVerified)
              return done(
                new UnauthorizedError(messages.login.response.notVerified),
              );
            else return done(null, jwtPayload);
          })
          .catch((error: any): any => {
            return done(error);
          });
      },
    );
  }

  public use(req: any, res: Response, next: NextFunction): any {
    return passport.authenticate(
      'jwt',
      { session: false },
      (error: Error, decodedToken: IDecodedToken, info: any): any => {
        if (error) return next(new ValidationError(error.message));

        if (!decodedToken) {
          let message = messages.token.log.invalid;

          if (info !== undefined && info.message === 'invalid signature')
            message = messages.token.response.invalid;

          if (info !== undefined && info.message === 'No auth token')
            message = messages.token.response.empty;

          this.log.warn(
            messages.login.log.failed +
              String(decodedToken ? decodedToken : '' + message),
          );
          return next(new UnauthorizedError(message));
        }

        req.decodedToken = decodedToken;
        return next();
      },
    )(req, res, next);
  }
}

export default Authentication;
