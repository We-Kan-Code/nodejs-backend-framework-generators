const messages: any = {
  errors: {
    create: '<%= modelName %> not created',
    find: '<%= modelName %> not found',
  },
};

export { messages };
