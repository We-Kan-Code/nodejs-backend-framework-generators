# Backend Framework - Generators

[Backend Framework](https://bitbucket.org/We-Kan-Code/nodejs-backend-framework-core/) project and model generators

# Installation

`npm install -g @wekancompany/generator`

# Quick Start

Simply run `wekan-create`

![wekan-create demo](https://bitbucket.org/We-Kan-Code/nodejs-backend-framework-generators/raw/master/docs/images/wekan-create-demo.png 'wekan-create demo')

**To create a project, select new project, provide a project name and where you want it setup.**

**To create a model (and its controllers and services), select new model, provide a model name and the location of your project root folder.**

_Keep the model names singular, the generator converts the name to its plural form where requried._

Check out our [wiki](https://bitbucket.org/We-Kan-Code/nodejs-backend-framework-generators/wiki/Home) for more information

