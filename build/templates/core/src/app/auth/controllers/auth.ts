import {
  Controller,
  Post,
  Res,
  Body,
  UseBefore,
  Param,
} from 'routing-controllers';
import { Response } from 'express';
import validator from 'middleware/validator';
import { User } from 'app/user/models/user';
import AuthService from '../services/auth';
import { httpStatus } from '@wekancompany/common';
import Authentication from 'middleware/authentication';

@Controller('/auth/users')
class AuthController {
  @Post('/login')
  @UseBefore(validator.validate(User, ['auth']))
  public async login(
    @Body() user: User,
    @Res() res: Response,
  ): Promise<void | {}> {
    return AuthService.authenticate(user)
      .then(
        (result: any): Response => {
          return res.status(httpStatus.ok).json({ data: { user: result } });
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  @Post('/refresh')
  public async refresh(
    @Body() req: any,
    @Res() res: Response,
  ): Promise<Response> {
    return AuthService.refresh(req.refreshToken)
      .then(
        async (result: any): Promise<{}> => {
          return res.status(httpStatus.ok).json({ data: { user: result } });
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  @Post('/reset-password')
  public async resetPassword(
    @Body() req: any,
    @Res() res: Response,
  ): Promise<Response> {
    return AuthService.resetPassword(req.email)
      .then(
        async (result: any): Promise<any> => {
          return res.status(httpStatus.noContent).end();
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  @Post('/:id/verification-code')
  public async sendVerificationCode(
    @Param('id') id: string,
    @Res() res: Response,
  ): Promise<Response> {
    return AuthService.sendOtp(id)
      .then(
        async (result: any): Promise<void> => {
          return res.status(httpStatus.noContent).end();
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  // TODO add custom error message to get one
  @Post('/:id/verify')
  public async verify(
    @Param('id') id: string,
    @Body() req: any,
    @Res() res: Response,
  ): Promise<Response> {
    return AuthService.verify(id, req.verificationCode)
      .then(
        async (result: any): Promise<{}> => {
          return res.status(httpStatus.ok).json({ data: { user: result } });
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  /**
   * @todo
   */
  @Post('/revoke')
  @UseBefore(validator.validate(User, ['auth']))
  @UseBefore(Authentication)
  public async revoke(@Res() res: Response): Promise<void | {}> {
    return res.status(httpStatus.noContent).end();
  }

  /**
   * @todo
   */
  @Post('/:id/logout')
  @UseBefore(Authentication)
  public async logout(
    @Param('id') id: string,
    @Res() res: Response,
  ): Promise<void | {}> {
    return res.status(httpStatus.noContent).end();
  }
}

export default AuthController;
