import { logLevels } from 'common/enums';
const prefix = '[Configuration]';

const messages: any = {
  init: {
    failed: `${prefix} Failed to initialize the logger`,
  },
  validation: {
    levels: {
      invalid: `${prefix} Please provide a valid log level [${Object.values(
        logLevels,
      ).toString()}]`,
    },
  },
};

export { prefix, messages };
