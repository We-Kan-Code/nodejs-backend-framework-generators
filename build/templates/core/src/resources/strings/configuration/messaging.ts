const prefix = '[Configuration]';

const messages: any = {
  validation: {
    sendgrid: {
      api: {
        empty: `${prefix} Please provide a sendgrid api key`,
      },
    },
  },
};

export { prefix, messages };
