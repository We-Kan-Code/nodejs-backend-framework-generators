const prefix = '[Boot]';

const messages: any = {
  boot: {
    start: `${prefix} Setting up environment`,
    settings: `${prefix} Initializing settings`,
    database: `${prefix} Connecting to the database`,
    server: `${prefix} Starting Server at`,
    success: `${prefix} Up and running`,
  },
};

export { messages, prefix };
