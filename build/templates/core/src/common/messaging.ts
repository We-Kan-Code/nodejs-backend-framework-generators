import firebase from 'firebase-admin';
import plivo from 'plivo';
import configuration from 'configuration/manager';
import mail = require('@sendgrid/mail');

class Messaging {
  private log!: any;

  public constructor() {
    this.log = configuration.log.logger;
  }

  public async sendNotification(
    registrationToken: string,
    payload: firebase.messaging.MessagingPayload,
    options: firebase.messaging.MessagingOptions,
  ): Promise<void> {
    firebase
      .messaging()
      .sendToDevice(registrationToken, payload, options)
      .then((response): void => {
        this.log.info(response.results[0].error);
        this.log.info(`Successfully sent message: ${response}`);
      })
      .catch((error): void => {
        this.log.error(`Error sending message: ${error}`);
      });
  }

  public async sendSms(from: string, to: string, message: string) {
    const client = new plivo.Client();
    client.messages.create(from, to, message).then((messageCreated: string) => {
      this.log.info(messageCreated);
    });
  }

  public async sendMail() {}

  public async sendVerificationMail(email: string, verificationCode: string) {
    mail.setApiKey(configuration.sendgrid.apiKey);
    const msg = {
      to: email,
      from: 'sanchanm@wekan.company',
      subject: 'Account verification',
      text:
        'Enter the verification code ' +
        verificationCode +
        ' to complete registration',
      html:
        'Enter the verification code <strong>' +
        verificationCode +
        '</strong> to complete registration',
    };
    mail.send(msg);
  }
}

export default Messaging;
