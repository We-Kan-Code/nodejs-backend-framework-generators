enum validEnvironments {
  development = 'development',
  production = 'production',
}

const logLevels: {
  [key: string]: number;
} = {
  trace: 10,
  debug: 20,
  info: 30,
  warn: 40,
  error: 50,
  fata: 60,
};

enum userStatus {
  notVerified = 0,
  verified = 1,
  forceResetPasswordOnLogin = 2,
}

export { validEnvironments, logLevels, userStatus };
