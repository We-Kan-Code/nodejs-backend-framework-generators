import { InternalServerError } from '@wekancompany/common';
import { prefix } from 'resources/strings/configuration/aws';
import AWS from 'aws-sdk';

class SecretsManager {
  public region!: string;
  public secretId!: string;

  public static async getSecrets(region: string, secretId: string) {
    const client = new AWS.SecretsManager({
      region,
    });

    return client
      .getSecretValue({ SecretId: secretId })
      .promise()
      .then((data): [string, string, string] | boolean => {
        /**
         * Depending on whether the secret is a string or binary,
         * one of these fields will be populated
         */
        if ('SecretString' in data) {
          const secrets: any = JSON.parse(String(data.SecretString));
          return [
            secrets.authSecret,
            secrets.sendGridApiKey,
            secrets.firebaseCredentials,
          ];
        } else {
          if (data.SecretBinary !== undefined) {
          }
        }
        return false;
      })
      .catch((error) => {
        throw new InternalServerError(`${prefix} ${error.message}`);
      });
  }
}

class Aws {
  public secrets!: SecretsManager;
}

export { Aws, SecretsManager };
