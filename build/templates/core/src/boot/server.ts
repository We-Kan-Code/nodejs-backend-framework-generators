import App from 'app/app';
import configuration from 'configuration/manager';

class Server {
  public static start(): Promise<{}> {
    const app = new App();
    app.createServer();
    return new Promise((resolve, reject): void => {
      resolve('success');
      app.server
        .listen(configuration.port, (): void => {
          resolve('success');
        })
        .on('error', (error: any): void => {
          reject(error);
        });
    });
  }
}

export default Server;
