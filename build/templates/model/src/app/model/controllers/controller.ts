import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  QueryParam,
  Req,
  Res, 
  UseBefore,
  Delete,
} from 'routing-controllers';
import { httpStatus, ValidationError } from '@wekancompany/common';
import { Response } from 'express';
import { <%= modelName %>  } from 'app/<%= modelNameLower %>/models/<%= modelNameLower %>';
import Authentication from 'middleware/authentication';
import <%= modelName %>Service from 'app/<%= modelNameLower %>/services/<%= modelNameLower %>';
import Validator from 'middleware/validator';
import { messages } from 'resources/strings/app/common';

@Controller('/<%= modelNameLower %>s')
class <%= modelName %>Controller {
  @Post()
	@UseBefore(Validator.validate(<%= modelName %>, ['create']))
	@UseBefore(Authentication)
  public async create<%= modelName %>(
    @Body() <%= modelNameLower %>: <%= modelName %>,
    @Res() res: Response,
  ): Promise<any> {
    return <%= modelName %>Service.create(<%= modelNameLower %>)
      .then(
        (result: any): Response => {
          return res
            .status(httpStatus.created)
            .json({ data: { <%= modelNameLower %>: { _id: result._id } } });
        },
      )
      .catch((error: any): any => Promise.reject(error));
  }

  @Patch()
  @UseBefore(Validator.validate(<%= modelName %>, ['update']))
  @UseBefore(Authentication)
  public async update<%= modelName %>(
    @Req() req: any,
    @Body() <%= modelNameLower %>: <%= modelName %>,
    @Res() res: Response,
  ): Promise<any> {
    return <%= modelName %>Service.updateOne({ email: req.decodedToken.email }, <%= modelNameLower %>)
      .then(
        (result: any): Response => {
          return res
            .status(httpStatus.ok)
            .json({ data: { <%= modelNameLower %>: result } });
        },
      )
      .catch((error: any): any => Promise.reject(error));
  }

  @Get('/:id')
  @UseBefore(Authentication)
  public async get<%= modelName %>(
    @Param('id') id: string,
    @QueryParam('fields') fields: string,
    @Res() res: Response,
  ): Promise<any> {
    return <%= modelName %>Service.getOne({ _id: id }, fields)
      .then(
        (result: any): Response => {
          return res
            .status(httpStatus.ok)
            .json({ data: { <%= modelNameLower %>: result } });
        },
      )
      .catch((error: any): any => Promise.reject(error));
  }

  @Get()
  @UseBefore(Authentication)
  public async get<%= modelName %>s(
    @QueryParam('fields') fields: string,
    @QueryParam('sort') sort: string,
    @QueryParam('offset') offset: number,
    @QueryParam('limit') limit: number,
    @Res() res: Response,
  ): Promise<any> {
    offset = offset === undefined && limit !== undefined ? 0 : offset;
    limit = limit === undefined && offset !== undefined ? 10 : limit;

    if (!['asc', 'desc'].includes(sort.split('|')[1]))
      throw new ValidationError(messages.errors.validate.sort);

    return <%= modelName %>Service.getMany({}, fields, sort, offset, limit)
      .then(
        (results: any): Response => {
          const pageNo = Math.round(offset / limit + 1);
          return res.status(httpStatus.ok).json({
            data: { <%= modelNameLower %>s: results[0] },
            meta: {
              pagination: {
                page: pageNo > results[1] ? results[1] : pageNo,
                total: results[1],
              },
            },
          });
        },
      )
      .catch((error: any): any => Promise.reject(error));
  }

  @Delete('/:id')
  @UseBefore(Authentication)
  public async delete<%= modelName %>(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ): Promise<void> {
    return <%= modelName %>Service.deleteOne(id)
      .then((result: any): void => {
        return res.status(httpStatus.noContent).end();
      })
      .catch((error: any): any => Promise.reject(error));
  }
}

export default <%= modelName %>Controller;
