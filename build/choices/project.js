"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inquirer_1 = __importDefault(require("inquirer"));
const ncp_1 = require("ncp");
const path_1 = __importDefault(require("path"));
const shelljs_1 = __importDefault(require("shelljs"));
const chalk_1 = __importDefault(require("chalk"));
const yargs_1 = __importDefault(require("yargs"));
const mkdirp = require("mkdirp");
const generateCoreQuestions = [
    {
        name: 'name',
        type: 'input',
        message: 'Project name:',
    },
    {
        name: 'location',
        type: 'input',
        message: 'Project destination:',
    },
];
let targetDir = process.cwd();
function generateCore() {
    inquirer_1.default.prompt(generateCoreQuestions).then((answers) => {
        answers = Object.assign({}, answers, yargs_1.default.argv);
        let projectName = String(answers['name']);
        let targetLocation = String(answers['location']);
        targetDir = path_1.default.join(targetLocation, projectName);
        projectName = projectName.charAt(0).toUpperCase() + projectName.slice(1);
        const templatePath = path_1.default.join(__dirname, '../templates/core');
        const log = path_1.default.join(targetDir, '/storage/logs');
        mkdirp(log, function (err) {
            if (err)
                console.error(err);
            else
                createProjectFromTemplate(templatePath, '');
        });
    });
}
exports.generateCore = generateCore;
function postProcess(path) {
    shelljs_1.default.cd(path);
    let cmd = '';
    if (shelljs_1.default.which('yarn')) {
        cmd = 'yarn';
    }
    else if (shelljs_1.default.which('npm')) {
        cmd = 'npm install';
    }
    if (cmd) {
        const result = shelljs_1.default.exec(cmd);
        if (result.code !== 0) {
            return false;
        }
    }
    else {
        console.log(chalk_1.default.red('No yarn or npm found. Cannot run installation.'));
    }
    return true;
}
function createProjectFromTemplate(templatePath, projectName) {
    try {
        console.log(chalk_1.default.green('Setting up templates ...'));
        ncp_1.ncp(templatePath, targetDir, function (err) {
            if (err) {
                return console.error(err);
            }
            console.log(chalk_1.default.green('Done!'));
            console.log('');
            console.log(chalk_1.default.green('Installing node modules ...'));
            postProcess(path_1.default.resolve(targetDir));
            console.log(chalk_1.default.green('Done!'));
        });
        return true;
    }
    catch (error) {
        console.log('');
        console.log(error.message);
        return false;
    }
}
//# sourceMappingURL=project.js.map