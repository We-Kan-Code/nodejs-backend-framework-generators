"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inquirer_1 = __importDefault(require("inquirer"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const chalk_1 = __importDefault(require("chalk"));
const yargs_1 = __importDefault(require("yargs"));
const util_1 = __importDefault(require("../util"));
const generateModelQuestions = [
    {
        name: 'name',
        type: 'input',
        message: 'Model name:',
        when: () => !yargs_1.default.argv['name'],
        validate: (input) => {
            if (/^([A-Za-z\-\_\d])+$/.test(input))
                return true;
            else
                return 'Model name may only include letters, numbers, underscores and hashes.';
        },
    },
    {
        name: 'location',
        type: 'input',
        message: 'Project root:',
    },
];
let targetDir = process.cwd();
let options = {
    modelName: '',
    templatePath: '',
};
function generateModel() {
    inquirer_1.default.prompt(generateModelQuestions).then((answers) => {
        answers = Object.assign({}, answers, yargs_1.default.argv);
        let modelName = String(answers['name']);
        let targetLocation = String(answers['location']);
        targetDir = targetLocation;
        modelName = modelName.charAt(0).toUpperCase() + modelName.slice(1);
        const templatePath = path_1.default.join(__dirname, '../templates/model');
        options = {
            modelName,
            templatePath,
        };
        if (createModelFromTemplate(templatePath, '')) {
            console.log('');
            console.log(chalk_1.default.green('Done. You can find the newly created model inside src/app/ of your project folder'));
        }
    });
}
exports.generateModel = generateModel;
const skipFiles = ['node_modules'];
function createModelFromTemplate(templatePath, modelName) {
    try {
        const filesToCreate = fs_1.default.readdirSync(templatePath);
        const modelNameLower = modelName.toLowerCase();
        const tmp = options.modelName;
        filesToCreate.forEach((file) => {
            const origFilePath = path_1.default.join(templatePath, file);
            const stats = fs_1.default.statSync(origFilePath);
            if (skipFiles.indexOf(file) > -1)
                return;
            if (stats.isFile()) {
                let contents = fs_1.default.readFileSync(origFilePath, 'utf8');
                contents = util_1.default(contents, {
                    modelName: tmp,
                    modelNameLower: tmp.toLowerCase(),
                });
                const writePath = path_1.default.join(targetDir, modelName, options.modelName.toLowerCase() + '.ts');
                if (!fs_1.default.existsSync(writePath)) {
                    fs_1.default.writeFileSync(writePath, contents, 'utf8');
                    console.log(writePath);
                }
            }
            else if (stats.isDirectory()) {
                const tmpFile = file;
                if (file == 'model')
                    file = tmp.toLowerCase();
                const dir = path_1.default.join(targetDir, modelNameLower, file);
                if (!fs_1.default.existsSync(dir)) {
                    fs_1.default.mkdirSync(dir, { recursive: true });
                    console.log(dir);
                }
                createModelFromTemplate(path_1.default.join(templatePath, tmpFile), path_1.default.join(modelNameLower, file));
            }
        });
        return true;
    }
    catch (error) {
        console.log('');
        console.log(error.message);
        return false;
    }
}
//# sourceMappingURL=model.js.map