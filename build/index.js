#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inquirer_1 = __importDefault(require("inquirer"));
const yargs_1 = __importDefault(require("yargs"));
const project_1 = require("./choices/project");
const model_1 = require("./choices/model");
const questions = [
    {
        name: 'Select one of the options below to start',
        type: 'list',
        choices: ['New project', 'New model'],
    },
];
inquirer_1.default.prompt(questions).then((answers) => {
    answers = Object.assign({}, answers, yargs_1.default.argv);
    if (String(answers['Select one of the options below to start']) == 'New project')
        project_1.generateCore();
    else
        model_1.generateModel();
});
//# sourceMappingURL=index.js.map