interface CliModelOptions {
  modelName: string;
  templatePath: string;
}

export { CliModelOptions };
