import inquirer from 'inquirer';
import { ncp } from 'ncp';
import path from 'path';
import shell from 'shelljs';
import chalk from 'chalk';
import yargs from 'yargs';
import mkdirp = require('mkdirp');

const generateCoreQuestions = [
  {
    name: 'name',
    type: 'input',
    message: 'Project name:',
  },
  {
    name: 'location',
    type: 'input',
    message: 'Project destination:',
  },
];

let targetDir = process.cwd();

function generateCore() {
  inquirer.prompt(generateCoreQuestions).then((answers) => {
    answers = Object.assign({}, answers, yargs.argv);

    let projectName = String(answers['name']);
    let targetLocation = String(answers['location']);
    targetDir = path.join(targetLocation, projectName);
    projectName = projectName.charAt(0).toUpperCase() + projectName.slice(1);
    const templatePath = path.join(__dirname, '../templates/core');

    const log = path.join(targetDir, '/storage/logs');

    mkdirp(log, function(err) {
      if (err) console.error(err);
      else createProjectFromTemplate(templatePath, '');
    });
  });
}

function postProcess(path: string) {
  shell.cd(path);

  let cmd = '';

  if (shell.which('yarn')) {
    cmd = 'yarn';
  } else if (shell.which('npm')) {
    cmd = 'npm install';
  }

  if (cmd) {
    const result = shell.exec(cmd);

    if (result.code !== 0) {
      return false;
    }
  } else {
    console.log(chalk.red('No yarn or npm found. Cannot run installation.'));
  }

  return true;
}

function createProjectFromTemplate(templatePath: string, projectName: string) {
  try {
    console.log(chalk.green('Setting up templates ...'));
    ncp(templatePath, targetDir, function(err) {
      if (err) {
        return console.error(err);
      }
      console.log(chalk.green('Done!'));
      console.log('');

      console.log(chalk.green('Installing node modules ...'));
      postProcess(path.resolve(targetDir));
      console.log(chalk.green('Done!'));
    });
    return true;
  } catch (error) {
    console.log('');
    console.log(error.message);
    return false;
  }
}

export { generateCore };
