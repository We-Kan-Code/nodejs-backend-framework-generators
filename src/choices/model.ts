import inquirer from 'inquirer';
import fs from 'fs';
import path from 'path';
import chalk from 'chalk';
import yargs from 'yargs';
import { CliModelOptions } from '../interfaces';
import render from '../util';

const generateModelQuestions = [
  {
    name: 'name',
    type: 'input',
    message: 'Model name:',
    when: () => !yargs.argv['name'],
    validate: (input: string) => {
      if (/^([A-Za-z\-\_\d])+$/.test(input)) return true;
      else
        return 'Model name may only include letters, numbers, underscores and hashes.';
    },
  },
  {
    name: 'location',
    type: 'input',
    message: 'Project root:',
  },
];

let targetDir = process.cwd();

let options: CliModelOptions = {
  modelName: '',
  templatePath: '',
};

function generateModel() {
  inquirer.prompt(generateModelQuestions).then((answers) => {
    answers = Object.assign({}, answers, yargs.argv);

    let modelName = String(answers['name']);
    let targetLocation = String(answers['location']);
    targetDir = targetLocation;
    modelName = modelName.charAt(0).toUpperCase() + modelName.slice(1);
    const templatePath = path.join(__dirname, '../templates/model');

    options = {
      modelName,
      templatePath,
    };

    if (createModelFromTemplate(templatePath, '')) {
      console.log('');
      console.log(
        chalk.green(
          'Done. You can find the newly created model inside src/app/ of your project folder',
        ),
      );
    }
  });
}

const skipFiles = ['node_modules'];

function createModelFromTemplate(templatePath: string, modelName: string) {
  try {
    const filesToCreate = fs.readdirSync(templatePath);
    const modelNameLower = modelName.toLowerCase();
    const tmp = options.modelName;

    filesToCreate.forEach((file) => {
      const origFilePath = path.join(templatePath, file);
      const stats = fs.statSync(origFilePath);

      if (skipFiles.indexOf(file) > -1) return;

      if (stats.isFile()) {
        let contents = fs.readFileSync(origFilePath, 'utf8');

        contents = render(contents, {
          modelName: tmp,
          modelNameLower: tmp.toLowerCase(),
        });

        const writePath = path.join(
          targetDir,
          modelName,
          options.modelName.toLowerCase() + '.ts',
        );
        if (!fs.existsSync(writePath)) {
          fs.writeFileSync(writePath, contents, 'utf8');
          console.log(writePath);
        }
      } else if (stats.isDirectory()) {
        const tmpFile = file;
        if (file == 'model') file = tmp.toLowerCase();

        const dir = path.join(targetDir, modelNameLower, file);

        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir, { recursive: true });
          console.log(dir);
        }

        // recursive call
        createModelFromTemplate(
          path.join(templatePath, tmpFile),
          path.join(modelNameLower, file),
        );
      }
    });
    return true;
  } catch (error) {
    console.log('');
    console.log(error.message);
    return false;
  }
}

export { generateModel };
