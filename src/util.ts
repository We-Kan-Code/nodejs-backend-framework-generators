import ejs from 'ejs';

export interface TemplateData {
  modelName: string;
  modelNameLower: string;
}

function render(content: string, data: TemplateData) {
  return ejs.render(content, data);
}

export default render;
