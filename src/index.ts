#!/usr/bin/env node
import inquirer from 'inquirer';
import yargs from 'yargs';
import { generateCore } from './choices/project';
import { generateModel } from './choices/model';

const questions = [
  {
    name: 'Select one of the options below to start',
    type: 'list',
    choices: ['New project', 'New model'],
  },
];

inquirer.prompt(questions).then((answers) => {
  answers = Object.assign({}, answers, yargs.argv);

  if (
    String(answers['Select one of the options below to start']) == 'New project'
  )
    generateCore();
  else generateModel();
});
